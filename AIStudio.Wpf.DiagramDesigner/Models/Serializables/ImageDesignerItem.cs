﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ImageDesignerItem : DesignerItemBase
    {
        public ImageDesignerItem()
        {

        }
        public ImageDesignerItem(ImageItemViewModel item) : base(item)
        {
            Icon = item.Icon;
            Connectors = new List<FullyCreatedConnectorInfoItem>();
            foreach (var fullyCreatedConnectorInfo in item.Connectors)
            {
                FullyCreatedConnectorInfoItem connector = new FullyCreatedConnectorInfoItem(fullyCreatedConnectorInfo);    
                Connectors.Add(connector);
            }
        }
    }
}
