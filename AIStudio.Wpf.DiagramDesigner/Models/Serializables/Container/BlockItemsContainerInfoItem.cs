﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{

    [Serializable]
    [XmlInclude(typeof(BlockItemsContainerInfoItem))]
    public class BlockItemsContainerInfoItem : SelectableItemBase
    {
        public BlockItemsContainerInfoItem()
        {
        }

        public BlockItemsContainerInfoItem(BlockItemsContainerInfo viewmodel) : base(viewmodel)
        {
            this.OnlyOneChild = viewmodel.OnlyOneChild;
            this.ChildFlag = viewmodel.ChildFlag;
            this.PhysicalItemWidth = viewmodel.PhysicalItemWidth;
            this.PhysicalItemHeight = viewmodel.PhysicalItemHeight;
            this.Parameter = new ConstParameterItem(viewmodel.Parameter);

            Children = new List<BlockDesignerItem>(viewmodel.Children.Select(p => new BlockDesignerItem(p)));
        }

        public bool OnlyOneChild
        {
            get; set;
        }

        public List<string> ChildFlag
        {
            get; set;
        }


        [JsonProperty(PropertyName = "ItemWidth")]
        [XmlAttribute("ItemWidth")]
        public double PhysicalItemWidth
        {
            get; set;
        }

        [JsonProperty(PropertyName = "ItemHeight")]
        [XmlAttribute("ItemHeight")]
        public double PhysicalItemHeight
        {
            get; set;
        }

        [XmlElement]
        public ConstParameterItem Parameter
        {
            get; set;
        }

        [XmlArray]
        public List<BlockDesignerItem> Children
        {
            get; set;
        }    
    }
}
