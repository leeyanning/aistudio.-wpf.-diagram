﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
	public static partial class PathGenerators
	{
		public static PathGeneratorResult Straight(IDiagramViewModel _, ConnectionViewModel link, PointBase[] route, PointBase source, PointBase target)
		{
			route = ConcatRouteAndSourceAndTarget(route, source, target);

			double sourceAngle = SourceMarkerAdjustement(route, link.GetSourceMarkerWidth(), link.GetSourceMarkerHeight());
			double targetAngle = TargetMarkerAdjustement(route, link.GetSinkMarkerWidth(), link.GetSinkMarkerHeight());

			DoShift(route, link);

			var paths = new string[route.Length - 1];
			for (var i = 0; i < route.Length - 1; i++)
			{
				paths[i] = FormattableString.Invariant($"M {route[i].X} {route[i].Y} L {route[i + 1].X} {route[i + 1].Y}");
			}

			return new PathGeneratorResult(paths, sourceAngle, route[0], targetAngle, route[route.Length - 1]);
		}
	}
}
