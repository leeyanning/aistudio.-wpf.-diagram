﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;
using SvgPathProperties;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class EraserDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public EraserDrawingDesignerItemViewModel()
        {
        }

        public EraserDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel) : base(root, DrawMode.Eraser, startPoint, colorViewModel, true)
        {
        }

        public EraserDrawingDesignerItemViewModel(IDiagramViewModel root, DrawMode drawMode, ColorViewModel colorViewModel, bool erasable) : base(root, drawMode, null, colorViewModel, erasable)
        {

        }

        public EraserDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public EraserDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points.LastOrDefault() - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }

                var geometry = new PathGeometry();
                var figure = new PathFigure { IsClosed = true, IsFilled = true };
                geometry.Figures.Add(figure);

                var radius = ColorViewModel.LineWidth / 2;
                var positiveX = radius * (point.X > Points[0].X ? 1 : -1);
                var positiveY = radius * (point.Y > Points[0].Y ? 1 : -1);

                figure.StartPoint = new Point(Points[0].X - positiveX, Points[0].Y - positiveY);

                var line = new LineSegment(new Point(Points[0].X + positiveX, Points[0].Y - positiveY), false);
                figure.Segments.Add(line);

                line = new LineSegment(new Point(point.X + positiveX, point.Y - positiveY), false);
                figure.Segments.Add(line);

                line = new LineSegment(new Point(point.X + positiveX, point.Y + positiveY), false);
                figure.Segments.Add(line);

                line = new LineSegment(new Point(point.X - positiveX, point.Y + positiveY), false);
                figure.Segments.Add(line);

                line = new LineSegment(new Point(Points[0].X - positiveX, Points[0].Y + positiveY), false);
                figure.Segments.Add(line);

                Geometry = geometry;
                Points[0] = point;
                Erase(Geometry);

                return true;
            }

            else
            {
                return false;
            }
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return base.OnMouseUp(sender, e);
        }

        private new bool Erase(Geometry geometry)
        {
            var empty = true;
            List<DrawingDesignerItemViewModelBase> deleteDrawGeometries = new List<DrawingDesignerItemViewModelBase>();
            foreach (var g in this.Root.Items.OfType<DrawingDesignerItemViewModelBase>())
            {
                if (g.Erase(geometry.CloneCurrentValue()))
                    deleteDrawGeometries.Add(g);
                else
                    empty = false;
            }

            foreach (var g in deleteDrawGeometries)
            {
                this.Root.Delete(g);
            }

            return empty;
        }
    }
}
