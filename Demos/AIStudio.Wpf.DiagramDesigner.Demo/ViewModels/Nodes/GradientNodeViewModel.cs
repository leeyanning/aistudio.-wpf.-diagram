﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class GradientNodeViewModel : BaseViewModel
    {
        public GradientNodeViewModel()
        {
            Title = "Gradient";
            Info = "A node with a gradient background.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50 };
            node1.ColorViewModel.FillColor.BrushType = BrushType.LinearGradientBrush;
            node1.ColorViewModel.FillColor.GradientStop = new System.Collections.ObjectModel.ObservableCollection<GradientStop>()
            {
                new GradientStop(Colors.Red, 0),
                new GradientStop(Colors.Yellow, 0.5),
                new GradientStop(Colors.Blue, 1),
            };
            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300 };
            node2.ColorViewModel.FillColor.BrushType = BrushType.RadialGradientBrush;
            node2.ColorViewModel.FillColor.GradientStop = new System.Collections.ObjectModel.ObservableCollection<GradientStop>()
            {
                new GradientStop(Colors.Red, 0),
                new GradientStop(Colors.Yellow, 0.5),
                new GradientStop(Colors.Blue, 1),
            };
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50 };
            node3.ColorViewModel.FillColor.BrushType = BrushType.DrawingBrush;
            DiagramViewModel.Add(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.RightConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            DiagramViewModel.Add(connector2);
        }
    }
}