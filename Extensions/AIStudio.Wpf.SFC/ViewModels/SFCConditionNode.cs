﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class SFCConditionNode : SFCNode
    {
        public SFCConditionNode() : this(null)
        {
           
        }

        public SFCConditionNode(IDiagramViewModel root) : base(root, SFCNodeKinds.Condition)
        {
            ColorViewModel.LineColor.Color = Colors.Black;
            ItemWidth = 30;
            ItemHeight = 30;

            ExecuteAddTopInput(null);
            ExecuteAddBottomOutput(null);
        }

        public SFCConditionNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public SFCConditionNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);
            CustomText = true;
        }

        protected override void InitNew()
        {
            base.InitNew();
        }

        private ObservableCollection<LinkPoint> _linkPoint = new ObservableCollection<LinkPoint>();
        public ObservableCollection<LinkPoint> LinkPoint
        {
            get
            {
                return _linkPoint;
            }
            set
            {
                SetProperty(ref _linkPoint, value);
            }
        }

        protected override void ExecuteEditCommand(object parameter)
        {
            SFCConditionNodeData data = new SFCConditionNodeData(LinkPoint, Expression);
            if (visualiserService.ShowDialog(data) == true)
            {
                this.LinkPoint = new ObservableCollection<LinkPoint>(data.LinkPoint.Select(p => SFCService.LinkPoint.FirstOrDefault(q => q.Name == p.Name)));
                this.Expression = data.Expression;
            }
        }
    }
}
